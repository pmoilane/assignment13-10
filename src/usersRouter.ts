import express from "express"
import { readAllUsers, readUser, addUser, deleteUser  } from "./dao"
import { Request, Response } from "express"

const usersRouter = express.Router()

usersRouter.get("/", async (req: Request, res: Response) => {
	const result = await readAllUsers()
	res.send(result)
})

usersRouter.get("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const user = await readUser(id)
	res.send(user)
})

usersRouter.post("/", async (req: Request, res: Response) => {
	const { username, full_name, email } = req.body
	const id = await addUser(username, full_name, email)
	res.send({ id, username, full_name, email })
})

usersRouter.delete("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	await deleteUser(id)
	res.status(200).send({ response: `Deleted user with id: ${id}`})
})

export default usersRouter