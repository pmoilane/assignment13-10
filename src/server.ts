import express from "express"
import usersRouter from "./usersRouter"
import postsRouter from "./postsRouter"
import commentsRouter from "./commentsRouter"

const server = express()
server.use(express.json())

server.use("/users", usersRouter)
server.use("/posts", postsRouter)
server.use("/comments", commentsRouter)

export default server