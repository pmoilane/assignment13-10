FROM node:18-alpine AS builder

COPY ./*.json ./
COPY ./src ./src

RUN npm ci
RUN npm run build

FROM node:18-alpine AS final

WORKDIR /app

ARG PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE

ENV PORT=${PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
ENV NODE_ENV="production"

COPY --from=builder ./dist ./dist
COPY ./package*.json ./

RUN npm ci --omit=dev

EXPOSE ${PORT}
CMD [ "npm", "start" ]