/* eslint-disable @typescript-eslint/no-explicit-any */
import request from "supertest"
import server from "../src/server"
import { pool } from "../src/db"

const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe("Testing GET /users", () => {
	const mockResponse = {
		rows: [
			{ id: 101, username: "Test user 1" },
			{ id: 101, username: "Test user 2" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with all users", async () => {
		const response = await request(server).get("/users")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe("Testing GET /users/:id", () => {
	const mockResponse = {
		rows: [
			{ id: 101, username: "Test user 1", full_name: "Firstname Lastname", email: "test@email.com" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with a user", async () => {
		const response = await request(server).get("/users/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})


describe("Testing POST /users", () => {
	const mockResponse = {
		rows: [
			{ id: 1, username: "Test user 1", full_name: "Firstname Lastname", email: "test@email.com" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with a user", async () => {
		const response = await request(server)
			.post("/users")
			.send({ username: "Test user 1", full_name: "Firstname Lastname", email: "test@email.com" })
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})

describe("Testing DELETE /users/:id", () => {
	const mockResponse = {
		rows: [
			{ response: "Deleted user with id: 101" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with response text with id of deleted user", async () => {
		const response = await request(server)
			.delete("/users/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})